import subprocess
from bottle import run, post, request, response, get, route

@route('/<path>',method = 'GET')
def process(path):
    return subprocess.check_output(['docker exec -it ajax /bin/bash /home/ajax/control/'+path+'.sh'],shell=True)

run(host='0.0.0.0', port=8080, debug=True)
