# ABANDONED. 
## I, like many before me, has moved to a more hardware based integration. This is not becuase this approach does not work. I used it myself for quite some time. But it requires quite a lot of juice, and some maintenence when new versions of the app being released so when there was a discount on space control i jumped ship.

### ajax-system-control

 * Clone repo, 
 * cd into directory.
 * create file includes/home/ajax/.authDetails and add variables username="YOURUSER" and password="YOURPASSWORD"
 * run `./start`

sample contents of .authdetails:
```
username="samleuser@example.com"
password="MYPASSWORD"
```

After installation is complete you can VNC into the container using port 5910 (which is exported).
To do so, run `vncviewer :5910` and follow the instructions on screen (click your way thru the installer)
When done, you will get an option to run the ajax desktop pro app.

Proceed and log into your pro-account (or create a new account)

After which you can start running scripts (on the host machine) like this:
docker exec -it ajax /bin/bash /home/ajax/control/{SCRIPT}.sh

Everything you need should be installed in the docker, but if you have imagemagick and tesseract and so on on your 

host machine you can run the scripts direcly (the host machine's X socket is shared.)

/bin/bash ./includes/home/ajax/control/{COMMAND}.sh

Available commands (at the time i'm writing this):

 * arm
 * disarm
 * getStatus (can take argument "collect")
 * getCurrentStatus (alias for getStatus with collect argument)
 * log
 * menu (takes arguments "control" and "log")
 * nightMode
 * panic
 * startup
