#!/bin/bash
cd /home/ajax;
CURRENT_PATH=$(dirname "$(readlink -f "$0")")
export DISPLAY=:1

if [[ ! -f "$CURRENT_PATH/.wine/installed" ]]; then
    wget "https://app.ajax.systems/prodesktop/get-windows" -O "$CURRENT_PATH/AjaxSetup.exe"
    /usr/bin/wine "$CURRENT_PATH/AjaxSetup.exe"
    touch "$CURRENT_PATH/.wine/installed"
fi

if [[ -f "$CURRENT_PATH/.wine/drive_c/Program Files/Ajax Pro/AjaxPro.exe" ]]; then
    /usr/bin/wine "$CURRENT_PATH/.wine/drive_c/Program Files/Ajax Pro/AjaxPro.exe"
fi

if [[ -f "$CURRENT_PATH/.wine/drive_c/Program Files (x86)/Ajax Pro/AjaxPro.exe" ]]; then
    /usr/bin/wine "$CURRENT_PATH/.wine/drive_c/Program Files (x86)/Ajax Pro/AjaxPro.exe"
fi

