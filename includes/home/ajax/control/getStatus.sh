#!/bin/bash

if [[ "$1" == "collect" ]]; then
  getStatus=$(DISPLAY=:1 \
    import -window root png:-| \
    convert \
      png:- \
      -type grayscale \
      -level 100%,30% \
      -crop 140x24+460+43 \
      -density 200 \
      -units PixelsPerInch \
      -| \
    tesseract -l eng stdin stdout|head -1);
  echo "$getStatus";
else
  if [[ -f ~/currentStatus ]]; then
    echo "$(cat ~/currentStatus|awk '{print $3}')"
  fi
fi
