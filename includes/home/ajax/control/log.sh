#!/bin/bash
if [[ ! -f ~/log.log ]]; then
  touch ~/log.log;
  . ~/control/subTasks/log.sh raw > ~/log.log;
fi

if [[ "force" == $1 ]]; then
  . ~/control/subTasks/log.sh raw > ~/log.log;
  . ~/control/menu.sh control
fi

if [[ "collect" == $1 ]]; then
  . ~/control/subTasks/log.sh raw > /tmp/currentlog.log;
  . ~/control/subTasks/logreader.sh current;
  . ~/control/menu.sh control
else
  . ~/control/subTasks/logreader.sh
fi
