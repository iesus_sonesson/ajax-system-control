#!/bin/bash
export DISPLAY=1;
getInfo=$(DISPLAY=:1 \
    import -window root png:-| \
    convert \
      png:- \
      -type grayscale \
      -level 100%,30% \
      -crop 420x100+320+310 \
      -density 200 \
      -units PixelsPerInch \
      -| \
    tesseract -l eng+swe stdin stdout|sed -e "s/^[[:space:]]*//");
detectError=$(echo "$getInfo"|head -1)
errorMessage=$(echo "$getInfo"|tail -1)

if [[ "Error" == $detectError ]]; then
  echo "$errorMessage"
  DISPLAY=:1 xdotool mousemove 560 390
  DISPLAY=:1 xdotool click 1
fi

if [[ "Attention!" == $detectError ]]; then
  echo "$errorMessage"
  DISPLAY=:1 xdotool mousemove 560 390
  DISPLAY=:1 xdotool click 1
fi
