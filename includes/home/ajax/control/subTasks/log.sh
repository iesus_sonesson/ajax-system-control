#!/bin/bash
    DISPLAY=:1 xdotool mousemove 60 200; DISPLAY=:1 xdotool click 1
    sleep 3
    log=$(DISPLAY=:1 import -window root png:-| \
    convert \
      png:- \
      -type grayscale \
      -level 100%,30% \
      -crop 600x700+165+100 \
      -chop 83x640+372+640 \
      -density 200 \
      -units PixelsPerInch \
      -| \
      tesseract stdin - -l eng+swe --user-words /etc/tesseract-words.txt --user-patterns /etc/tesseract-pattern.txt --psm 6);
log=$(echo "$log"|awk 'NF > 0');

if [[ "$1" == "raw" ]]; then
  echo "$log"
else 
  echo "$log" | column
fi
