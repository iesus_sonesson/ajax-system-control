#!/bin/bash
if [[ "current" == "$1" ]]; then
  log=/tmp/currentlog.log
else
  log=~/log.log
fi

readarray logEntries < <(head -8 $log)
for i in "${logEntries[@]}"; do :
  i=$(sed -e 's/^[[:space:]]*//' <<< $i)
  i=$(sed -e 's/^\\$//' <<< $i)
  if [[ -z "$i" ]]; then  
    continue; 
  fi;
  t=${i: -5};
  t=$(sed -e 's/^[[:space:]]*//' <<< $t)
  i=$(echo "$i" | sed "s/$t//g" 2>/dev/null)
  if [[ ${#t} == 4 ]]; then
    t="${t:0:2}:${t: -2}"
  fi
  echo "$t $i" 
done;
