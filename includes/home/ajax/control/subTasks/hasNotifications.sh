#!/bin/bash
export DISPLAY=:1

function detectedNotifications() {
  getNotification=$(import -window root png:- | \
      convert \
        png:- \
        -crop 1x1+220+205 \
        -define histogram:unique-colors=true \
        -format %c histogram:info:- \
      |sed -e 's/^[[:space:]]*//' \
      |awk '{ print $2 $3 $4 }' \
      |tail -c +2 \
      |head -c -2);
  r=$(echo $getNotification |cut -d "," -f 1);
  g=$(echo $getNotification |cut -d "," -f 2);
  b=$(echo $getNotification |cut -d "," -f 3);

  if [[ "$r" -gt 100 ]]; then
   return 0;
  fi

  return -1
}

if detectedNotifications ;then
  $(. ~/control/subTasks/log.sh raw >> /home/ajax/log.log)
  $(. ~/control/menu.sh control)
fi
