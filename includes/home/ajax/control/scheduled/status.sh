#!/bin/bash

run() {
  datestamp=$(date +'%Y-%m-%d %H:%M:%S')
  status=$(/bin/bash ~/control/getStatus.sh collect)
  echo "$datestamp $status" > ~/currentStatus
}

runner() {
  while :; do
    run
    sleep 10
  done;
}
runner;
