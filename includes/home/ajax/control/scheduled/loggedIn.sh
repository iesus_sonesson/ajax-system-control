#!/bin/bash

if [[ ! -f ~/.authDetails ]]; then
  echo "no auth file found."
  exit;
fi

. ~/.authDetails

checkStatus() {
  log=$(DISPLAY=:1 import -window root png:- | \
    convert \
      png:- \
      -type grayscale \
      -level 100%,30% \
      -crop 450x100+130+100 \
      -density 200 \
      -units PixelsPerInch \
      - | \
    tesseract stdin - -l eng)
  log=$(echo "$log"|sed -e 's/^[[:space:]]*//');
  if [[ "$log" == "AUTHORIZATION" ]]; then

    return 0
  fi

  return 1
}

checkIfMonitoringState() {
  monitoring=$(DISPLAY=:1 \
    import -window root png:-|\
    convert \
      png:- \
      -type grayscale \
      -level 100%,30% \
      -crop 140x24+237+43 \
      -density 200 \
      -units PixelsPerInch \
      -| \
    tesseract -l eng stdin stdout|head -1);
  if [[ "$monitoring" == "Monitoring" ]]; then

    return 0
  fi

  return 1
}

doLogin() {
  export DISPLAY=:1
  xdotool mousemove 420 320
  xdotool click --repeat 3 1;
  xdotool type "$username"
  xdotool mousemove 420 370
  xdotool click --repeat 3 1
  xdotool type "$password"
  xdotool mousemove 435 410
  xdotool click 1
  xdotool mousemove 550 500
  xdotool click 1
}

run() {
  if checkStatus; then
    doLogin
  fi

  if checkIfMonitoringState; then
    /bin/bash ~/control/startup.sh
  fi
}

runner() {
  while :; do
    run
    sleep 30
  done;
}
runner;
